# GS15-Blockchain

Projet de cryptographie a l'Universite de Technologie de Troyes - Flavien Belli - Paul Lefranc

## Fonctionnement
A la racine du dossier, lancer `python3 main.py`.   
Il est possible qu'il vous soit demande d'installer des libraries python : 
dans ce cas la, importer `Natsort`,`libnum` et `pyfinite`.
Le reste des librairies doit etre present nativement au sein de python3.

Certaines fonctionnalités nécessitent la présence d'un fichier texte spécifique dans le dossier files:
- Le texte à chiffrer avec Kasumi doit être placé dans un fichier "input.txt"
- Le texte à déchiffrer avec Kasumi doit être placé dans un fichier "crypted.txt", notez que la fonction de chiffrement créera ce fichier automatiquement
- Le texte à hasher doit être placé dans un fichier "tohash.txt"
- Le fichier à vérifier avec le hash doit être placé dans un fichier "verif.txt"

La blockchain doit être initialisée une première fois avant d'ajouter un bloc ou d'effectuer la vérification. Cela permet à la blockchain existante dans les fichiers de s'injecter dans le programme. Sans initialisation, le bloc créé va remplacer le fichier `/blockchain/0.txt` et casser la blockchain créée pour la soutenance. 

## Choix d'implémentation

Les utilisateurs de la blockchain sont des objets initialisés au debut de `main.py` de manière statique : Leurs identifiants, noms et clés sont enregistrés dans le code et ne doivent pas etre modifiés.

Pour la signature de la blockchain, afin de faciliter la vérification, nous sommes partis sur la signature ElGamal, puisque nos utilisateurs sont 
des objets avec, pour attributs, leurs clés publiques et privées. Ainsi la signature et la verification se fait sans d'autres variables intermediares necessitant un stockage.

Pour gagner du temps, nous avons choisi d'utiliser le SHA-256 que nous avons implémenté plutôt que la fonction éponge pour la Blockchain. La preuve de calcul utilisée pour créer un bloc est donc plus rapide avec SHA-256. La création de bloc est ainsi beaucoup plus rapide. Aussi, nous avons ajouté la possibilité de hasher en SHA-256 pour les menus 4, 5 et 6. Le fichier `hashage.py` est donc séparé en deux parties : L'implémentation de SHA-256 puis l'implémentation de la fonction éponge utilisant un SHA-256 modifié.

## Difficultés rencontrées

Pour la fonction FL de Kasumi, nous avons utilisé la fonction d'inversion dans un corps de Gallois de `pyfinite` car nous n'avons pas réussi à trouver et utiliser un élément générateur pour générer le corps cyclique. 

La détermination d'un générateur a également posé problème au début du projet pour le protocole Diffie-Hellman, mais grace à votre aide ce problème a été rapidement surmonté.

Nous avons également eu quelques difficultés avec la gestion du binaire et du typage des variables en python, nous avons donc procédé à beaucoup de conversions entre des caractères, des entiers, du hex et du binaire. Il est donc possible que le code ne soit pas optimisé au niveau de la mémoire utilisée.  

À l'origine, les utilisateurs étaient créés à la volée lors du lancement de la blockchain. Cependant, l'attribution de clé publique et privée était aléatoire et la consistence des signatures dans les transactions n'était plus valable, ce qui "cassait" notre blockchain. Nous avons donc finalement décidé de générer une fois les utilisateurs de manière aléatoire, mais de stocker ces objets au début de `main.py` pour conserver la validité des signatures.

Enfin, nous ne savons pas si les blocs de la blockchain sont de taille constante, nous avons pris le choix de permettre a l'utilisateur de rentrer autant de transaction qu'il souhaite dans un bloc avant de passer automatiquement à un autre bloc une fois que la fonction est relancée. 