# Main.py ######
#
# Launch this file to access to the project
#
#
# Imports
import utils.kasumi as kasumi
import utils.Diffie as Diffie
import utils.hashage as hashage
import utils.signature as signature
import utils.generator as generator
import utils.blockchain as blockchain
import os
from os import path
import time


## Definition de la fonction de nettoyage de la console en fonction de l'OS
if os.name == "posix":
    clear = lambda: os.system("clear")
else: clear = lambda: os.system("cls")


## Récupération des paramètres P et Gen pour les algorithmes
try :
    open("./files/Alicekey.txt", "r")
    P, gen = Diffie.getPandGen()
except:
    P, gen = Diffie.generatePandGen()

## Création des utilisateurs de la blockchain (Paul, Flavien, Remi, Nicolas)
## Les noms choisis et les sommes versés aux différents enseignants sont purement fortuites

paul = blockchain.User(0, "Paul", 9294234111431459816223486585781698681171867612149292023362896910552481473006985919511480890796929804378398323997027674532245931095638430945647511964549519, 11288051376277965838919607388645568609319465645572097945454681570720965724808014016382631749632913479315578289605107725616460134498150828779376119469317025)
flavien = blockchain.User(1, "Flavien", 6759289238948564958386852903548832306512818837901163564366338615463355883622066500700046414742839832042535957748244839589869571867720381747884220970845544, 12112777910747145739093164264966354685439434429877658784342447271009781127071097191152920337505264570632686724645403704060850054744042096684072295908528883)
remi = blockchain.User(2, "Remi", 3680891820811327215243106309374563423179378239054276894752786883043314477410992532067032446545055813359135400806410895405328016609016830186721503513730717, 2042374601235960188048034714086183657563842788902093598463178883207162704447751962855441946978631529978000257676801175404282285970778936198427806637479044)
nicolas = blockchain.User(3, "Nicolas", 1616218280316717365758141184496378191425546127115418394384119422281077817768044799953570552118601592139559522269616420623850564282134525240192531901687887, 8891433756064183219861285955697413214928429873531398966682567251389412415787207470559718434648846101510971919879180977723485644503073482719347982671322960)
users = (paul, flavien, remi, nicolas)
#Création de la blockchain
newblockchain = blockchain.Blockchain()

#
#
print("Bonjour, bienvenue dans le seul et unique createur de bUTTcoin")
running = True
while(running):
    print("Que voulez vous faire ?")
    # Print all the options
    print("->1<- Chiffrer un message.")
    print("->2<- Déchiffrer un message.")
    print("->3<- Générer des couples de clés publiques / privées.")
    print("->4<- Générer un hash / une empreinte.") #N doit être > 0
    print("->5<- Vérifier un hash / une empreinte.")
    print("->6<- Effectuer une preuve de travail.")
    print("->7<- Vérifier une transaction (une signature).")
    print("->8<- Débuter / incrémenter la Block-chain.")
    print("->9<- Vérifier l’intégrité de la block-chain.")
    print("->10<- I WANT IT ALL !! I WANT IT NOW !!")
    print("->11<- DONNER UNE EXCELLENTE NOTE À CE PROJET")
    print("->12<- Quitter ce magnifique programme")
    #
    #
    # launch program according to the option chosed by user
    choice = input("Votre choix : ")

    if choice=="1":
        if path.exists("files/input.txt"):
            mode = input("Indiquer le mode de chiffrement (ECB, CBC ou PCBC) : ")
            if ((mode!="ecb") and (mode!="ECB") and (mode!="cbc") and (mode!="CBC") and (mode!="pcbc") and (mode!="PCBC")):
                print("Le mode " + mode + ", n'est pas valide")
            else:
                key = input("Rentrer la clé : ")
                print(kasumi.encryptKasumi(mode, key))
            input("------------")
            clear()
        else:
            print("Le message à crypter doit être placé dans un fichier \"input.txt\" dans le dossier files")
            input("------------")
            clear()

    elif choice=="2":
        if path.exists("./files/crypted.txt"):
            mode = input("Indiquer le mode de déchiffrement (ECB, CBC ou PCBC) : ")
            if ((mode != "ecb") and (mode != "ECB") and (mode != "cbc") and (mode != "CBC") and (mode != "pcbc") and (
                    mode != "PCBC")):
                print("Le mode " + mode + ", n'est pas valide")
            else:
                key = input("Rentrer la clé : ")
                print(kasumi.decryptKasumi(mode, key))
            input("------------")
            clear()
        else:
            print("Le message à décrypter doit être placé dans un fichier \"crypted.txt\" dans le dossier files")
            input("------------")
            clear()

    elif choice == "3":
        Diffie.menu(P, gen)
        input("------------")
        clear()
    elif choice == "4":
        try:
            open("./files/tohash.txt", "r").read()
            hash, toprint = hashage.menu("./files/tohash.txt")
            print(toprint + " : " + hash)
            open("./files/hash.txt", "w").write(hash)
            print("Le hash peut être retrouvé dans le fichier \"hash.txt\" dans le dossier files")
            input("------------")
            clear()
        except FileNotFoundError:
            print("Le fichier à Hasher doit être un fichier \"tohash.txt\" dans le dossier files")
            input("------------")
            clear()
    elif choice == "5":
        try:
            open("./files/verif.txt", "r").read()
            hash, toprint = hashage.menu("./files/verif.txt")
            hashUser = input("Merci d'entrer le hash permettant de vérifier le fichier : ")
            if(hash == hashUser):
                print("Le hash entré et le hash du fichier sont les mêmes")
            else:
                print("Le hash entré est différent du hash du fichier. Attention, le fichier a peut-être été modifié !")
            input("------------")
            clear()
        except FileNotFoundError:
            print("Le fichier à vérifier doit être un fichier \"verif.txt\" dans le dossier files")
            input("------------")
            clear()
    elif choice =="6":
        try:
            message = open("./files/tohash.txt", "r").read()
            hashage.proofOfWork(message)
            input("------------")
            clear()
        except FileNotFoundError:
            print("Le fichier à tester doit être un fichier \"tohash.txt\" dans le dossier files")
            input("------------")
            clear()
    elif choice == "7":
        signature.menu1(P, gen)
        input("------------")
        clear()
    elif choice == "8":
        choice8 = input("Que voulez vous faire (La blockchain doit d'abord être initialisée une première fois avant d'ajouter un bloc. Sinon, le bloc va se créer à la position 0 au lieu de se placer à la suite de la blockchain existente) ? \n->1<- Initialiser la blockchain \n->2<- Ajouter un bloc \nVotre choix : ")
        if choice8 == "1":
            newblockchain.initBlockchain()
            print("La blockchain est initialisée")
            input("------------")
            clear()
        elif choice8=="2":
            newblockchain.createNewBlock(users)
            input("------------")
            clear()
    elif choice == "9":
        newblockchain.verifyBlockchain(users)
        input("------------")
        clear()
    elif choice=="10": #Petite blague :)
        paroles = open("./files/iwantitall.txt", "r").read().splitlines()
        for index, l in enumerate(paroles):
            print(l)
            try:
                print(paroles[index + 1])
                try:
                    print(paroles[index + 2])
                    time.sleep(3)
                    clear()
                except:
                    print("Please subscribe to my chanel")
            except:
                print("")
                clear()
        input("------------")
        clear()
    elif choice=="11":
        print("En temps que Grand maître de GS15, vous ressentez l'irrésistible besoin d'attribuer une excellente note à Flavien et Paul (svp)")
        input("------------")
        clear()
    elif choice == "12":
        print("Merci, au revoir")
        running = False
    elif choice == "13":
        generator.primefile()
    else:
        print("WIP ou mauvaise commande")

