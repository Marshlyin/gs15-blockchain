def removeDuplicateFromList(x):
    """
    :param x: the list with duplicate
    :return: the same list cleaned
    """

    return list(dict.fromkeys(x))