import utils.hashage as hashage
import utils.Diffie as Diffie
import utils.generator as generator
from math import gcd
import random
import libnum

global P, gen

####### The menus of the signature

def menu1(P, gen):
    choix = input("Que voulez vous faire ? \n->1<- Générer une signature ElGamal \n->2<- Générer une signature RSA\nVotre choix : ")
    if choix == "1"or choix == "2":
        menu2(choix, P, gen)
    else :
        print("Entrée invalide... Réessayez")
        menu1(P, gen)

def menu2(choix1, P, gen):
    choix2 = input("Que voulez vous faire ? \n->1<- Entrer un message a signer \n->2<- Signer un message dans le fichier texte\nVotre choix : ")
    if choix2 == "1":
        message = input("Entrez le message à signer : ")
    elif choix2 == "2":
        try:
            message = open("./files/tohash.txt", "r").read()
        except FileNotFoundError:
            print("Le fichier à tester doit être un fichier \"tohash.txt\" dans le dossier files")
            input("------------")
            exit()
    else:
        print("Entrée invalide... Réessayez")
        menu2(choix1, P, gen)
        exit()

    if choix1 == "1":
            sign = ElGamalSign(message, P, gen)
            ElGamalVerify(sign, message, P, gen)
            saveSignatureElGamal(message, sign)
    elif choix1 == "2":
            sign, n, e = RSAsign(message)
            RSAverify(message, sign, n, e)
            saveSignatureRSA(message, sign, n, e)




def ElGamalSign(message, P, gen):
    """
    Alice signs a message using ElGamal Signature and send it to bob with her signature
    :param message: the message alice wants to send
    :param P: the strong prime number, parameter of all the program
    :param gen: its generator
    :return: the signature which is a table of the two parts, R (0) and S (1)
    """
    print("Les paramètres de l'algorithme sont P = %s et le générateur G = %s" %( P,gen))
    kpub, kprivAlice = Diffie.getAliceKeys()
    print("La clé publique d'Alice est %s et sa clé privée est %s" %(str(format(kpub, 'x')) , str(format(kprivAlice, "x"))))
    print("Elle hashe le fichier en sha-256")
    hash = hashage.sha256(message)
    print("Le hash du message est : "+hash)
    print("Alice choisi aléatoirement un entier plus petit que P-2, premier avec lui")
    k = random.randint(0, P-2)
    while gcd(k, P-1) != 1:
        k = random.randint(0, P - 2)
    r = pow(gen, k, P)
    s = ((int(hash, 16) - kprivAlice*r) * libnum.invmod(k, P-1)) % (P-1)
    signature = r, s
    print("La signature du fichier d'Alice est donc : %s , %s" %(str(format(signature[0], 'x')), str(format(signature[1], 'x'))))
    return signature

def ElGamalVerify(signature, message, P, gen):
    """
    Bob verifies the message Alice sent him with the signature
    :param signature: the signature alice just created
    :param message: the message bob wants to verify
    :param P: the strong prime number, parameter of all the program
    :param gen: its generator
    :return: true if the message is verified, false otherwise
    """
    print("Bob récupère le message et la signature envoyée par Alice : %s , %s" %(str(format(signature[0], 'x')), str(format(signature[1], 'x')) ))
    r, s = signature
    print("Bob connait les paramètres de l'algorithme P et Gen")
    kpub, kprivAlice = Diffie.getAliceKeys()
    print("Bob connait la clé publique d'Alice %s" %(str(format(kpub, 'x'))))
    hash = hashage.sha256(message)
    print("Bob calcule le hash du message d'Alice : %s" %hash)
    v_1 = (pow(kpub, r, P) * pow(r, s, P)) % P
    v_2 = pow(gen, int(hash, 16), P)
    print("Bob vérifie que les infos de la signature %s correspondent au fichier reçu : %s" %(str(format(v_1, 'x')) , str(format(v_2, 'x'))))
    if v_1 == v_2:
        print("Le fichier recu est bien celui envoyé par Alice ! Youpi")
        return True
    else :
        print("Eve a du passer par là...")
        return False


def saveSignatureElGamal(message, signature):
    """
    We save the message and its signature in a file
    :param message: the message Alice sent to Bob
    :param signature: the signature that went with the message
    :return: the signature is saved in the signature.txt file
    """
    try :
        print("Enregistrement dans le fichier...")
        with open("./files/signature.txt", "a") as f:
            f.write("%s, %s, %s \n" % (message, signature[0], signature[1]))
        f.close()

    except :
        print("Le fichier n'existe pas")
        print("Création et enregistrement dans le fichier...")
        with open("./files/signature.txt", "w") as f:
            f.write("%s, %s, %s \n" %(message, signature[0],signature[1]))
        f.close()


def RSAsign(message):
    """
    Alice creates a message and create its signature using the RSA algorithm
    :param message: the message alice wants to send
    :return: the signature and the two parameters e and n of the algorithm
    """
    print("Alice génère deux nombres premiers de 512 bits...")
    p = int(generator.getPrimeFromFile())
    q = int(generator.getPrimeFromFile())
    print("Elle a donc choisi %s et %s" %(str(format(p, 'x')), str(format(q, 'x'))))
    n = p*q
    phi = (p-1)*(q-1)
    e = random.randint(1, phi)
    while (e%2!=1 and gcd(e, phi)!= 1):
        e = random.randint(1, phi)
    print("Sa clé publique est donc %s, %s" %(str(format(n, 'x')),(str(format(e, 'x')))))
    d = pow(e, -1, phi)
    print("Sa clé privée est alors %s" %(str(format(d, 'x'))))
    hash = hashage.sha256(message)
    sign = pow(int(hash, 16), d, n)
    print("Alice signe le message et l'envoie avec sa clé publique")
    return sign, n, e

def RSAverify(message, sign, n, e):
    """
    Bob verifies the message he gets from Alice using the RSA signature and its two parameters
    :param message: the message Alice sent him
    :param sign: the signature going with the message
    :param n: the n parameter of the RSA algorithm
    :param e: the e parameter of the RSA algorithm
    :return: true if the message is from Alice, false otherwise
    """
    print("Bob reçoit le message d'Alice et verifie les info de la signature...")
    if pow(sign, e, n) == int(hashage.sha256(message), 16)%n:
        print("La signature correspond bien à celle d'Alice pour le message reçu ! Youpi")
        return True
    else :
        print("Eve a du passer par là...")
        return False

def saveSignatureRSA(message, signature, n, e):
    """
    We save the message and its signature in a file
    :param message: the message Alice sent to Bob
    :param signature: the signature that went with the message
    :param n: the parameter n of the RSA signature
    :param e: the parameter e of the RSA signature
    :return: the signature is saved in the signature.txt file
    """
    try :
        print("Enregistrement dans le fichier...")
        with open("./files/signature.txt", "a") as f:
            f.write("%s, %s, %s, %s \n" % (message, signature, n, e))
        f.close()

    except :
        print("Le fichier n'existe pas")
        with open("./files/signature.txt", "w") as f:
            f.write("%s, %s, %s, %s\n" %(message, signature, n, e))
        f.close()