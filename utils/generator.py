# This file is used to store all function used to generate numbers, keys, etc.

from random import getrandbits, randrange, randint
from collections import Counter
import utils.global_function as global_function
from itertools import groupby
import sys


##### PRIME #####

### Prime generation ###

def generatePandGen():
    P = getPrimeFromFile()
    gen = findGeneratorStrong(P)
    return P, gen

# return a prime number (i.e odd) of the bitsize wanted
def generatePrimeCandidate(bitsize):
    """
    :param bitsize:
    :return:
    """
    p = getrandbits(bitsize)
    p |= (1 << bitsize - 1) | 1
    return p

def isPrime(n):
    """
    Test if the prime candidate is prime thanks to rabin miller
    :param n: the prime candidate
    :return:  true if n is prime, false otherwise
    """

    # Test if n is not even (or 2).
    if n == 2 or n == 3:
        return True
    if n <= 1 or n % 2 == 0:
        return False
    # find r and s
    s = 0
    r = n - 1
    while r & 1 == 0:
        s += 1
        r //= 2

    # do 64 tests
    for _ in range(64):
        a = randrange(2, n - 1)
        x = pow(a, r, n)
        if x != 1 and x != n - 1:
            j = 1
            while j < s and x != n - 1:
                x = pow(x, 2, n)
                if x == 1:
                    return False
                j += 1
            if x != n - 1:
                return False

    return True

def isStrongPrime(p):
    """
    :param p: the prime number to verify
    :return: true is it's a strong prime (p = 2q+1), false otherwise
    """

    q = (p-1) // 2
    return True if isPrime(q) else False


def generateStrongPrimeNumber(bitsize):
    """
    :param bitsize: the size (in bits) of the desired prime number
    :return: a strong prime number of the size wanted
    """

    p = generatePrimeCandidate(bitsize)
    while not (isPrime(p) and isStrongPrime(p)):
        p = generatePrimeCandidate(bitsize)
    return p

### Factorisation ###

def primefactor(n):
    """
    :param n: the number you want to have the prime factorisation
    :return: the prime factorisation
    """
    p = 2
    primeTable = []
    while n >= p * p:
        if n % p == 0:
            primeTable.append(p)
            n = n / p
        else:
            p = p + 1
    primeTable.append(int(n))

    return primeTable

def divisors(n):
    """
    :param n: the number you want to have the divisors
    :return: a table of all the divisors of n
    """
    pf = primefactor(n)
    recurringFactor = Counter(pf)
    print(recurringFactor)
    testN = 1
    testT = global_function.removeDuplicateFromList(pf)
    for prime, power in recurringFactor.items():
        testN *= prime**power
        print(testN)
        [testT.append(prime**(i+1)) for i in range(power) if prime**(i+1) not in testT]
    print(testN, testT)

def allPossibleFactors(p):
    """
    :param p: the prime you want the factors
    :return: a table of all possible factors (APF)
    """
    # based on the lagrange theorem, we calculate the factors of m (=p-1)
    m = p-1

    #pfm = the prime factor of m
    pfm = primefactor(m)

    #we create a dictionnary [prime, power]
    recurringFactor = [(i, pfm.count(i)) for i,_ in groupby(pfm)]

    # apf = allPossibleFactors
    apf = []

    # for each factor
    for i in range(len(recurringFactor)):
        #print(recurringFactor[i])
        key, val = recurringFactor[i]
        # for the number of time each factor appear
        for ii in range(val):
            temp = (key)**(ii+1)
            if temp > (m/2) : continue
            else :
                apf.append(temp)
                # we get each combination of the first factor and the combination left
                for j in range((i+1), len(recurringFactor)):
                    temp = (key) ** (ii + 1)
                    if temp > (m / 2): continue
                    else :
                        kj, vj = recurringFactor[j]
                        for jj in range(vj):
                            if (temp*kj) > (m / 2): continue
                            else :
                                temp *= kj
                                temp2 = temp
                                apf.append(temp)
                                for h in range((j + 1), len(recurringFactor)):
                                    kh, vh = recurringFactor[h]
                                    for hh in range(vh):
                                        if (temp2 * kh) > (m / 2): continue
                                        else : temp2 *= kh
                                        apf.append(temp2)
    #on retourne apf dans l'ordre croissant (plus rapide pour les calculs de générateur)
    return (sorted(apf))

def findGeneratorStrong(p):
    """
    :param p: the strong prime to find generator
    :return: the generator
    """
    # Since p is a strong prime (ie p = 2q+1), Lagrange theorem says that the gen can be found where a^2 != 1mod p and a^q != 1mod p

    q = (p - 1) // 2
    for a in range(2, p):
        i = pow(a,2,p)
        j = pow(a,q,p)
        if i != 1 and j != 1:
            return a


###### SBOX ########

def generateSBox1(key):
    """
    :param key: the key of the SBOX
    :return:  an SBOX created with RC4 algorithm
    """
    key = [ord(c) for c in key]
    state = [n for n in range(256)]
    #print(state)
    j = 0
    for i in range(256):
        if len(key) > 0:
            j = (j + state[i] + key[i % len(key)]) % 256
        else:
            j = (j + state[i]) % 256
        state[i], state[j] = state[j], state[i]
    return state

def generateSBox2(key):
    """
        :param key: the key of the SBOX
        :return:  an SBOX created with RC4 algorithm different from the 1st one
        """
    key = [ord(c) for c in key]
    state = [n for n in range(256)]
    #print(state)
    j = 0
    for i in range(256):
        if len(key) > 0:
            j = (j - state[i] + key[i % len(key)]) % 256
        else:
            j = (j - state[i]) % 256
        state[i], state[j] = state[j], state[i]
    return state

## To get the prime faster, we have created them before and stored them in a file
def primefile():
    """
    We create strong prime numbers and store them in a file
    :return:
    """
    #it's a while true function because it's only called when i was doing something else and let the program do its thing
    while 1 :
        p = generateStrongPrimeNumber(512)
        print(p)
        with open("./files/primes512.txt", "a") as f:
            f.write(str(p)+"\n")
        f.close()


def getPrimeFromFile():
    """
    We get a random prime from the file
    If the file doesnt exist, we create a prime from scratch and return it
    :return: a prime number of 512 bits
    """
    try :
        with open("./files/primes512.txt", "r") as f :
            primelist = f.read().splitlines()

        return primelist[randint(0, len(primelist)-1)]
    except :
        print("Le fichier n'existe pas... \nGénération d'un premier de 512 bits\nThis might take a while...")
        return generateStrongPrimeNumber(512)

