import utils.generator as generator
import sys
from pyfinite import ffield


"""
Définition des clés et sous-clés
"""
keyL1 = [None]*9
keyL2 = [None]*9

keyO1 = [None]*9
keyO2 = [None]*9
keyO3 = [None]*9

keyI1 = [None]*9
keyI2 = [None]*9
keyI3 = [None]*9

key = [None]*9
key_prime = [None]*9

master_key = 0x00000000000000000000000000000000
def setkeys(pkey):
    """
    KeyScheduling pour Kasumi tel que décrit dans la norme fournie par le 3GPP
    :param pkey: String. Clé de chiffrement
    :return: Void.
    """

    master_key = int(pkey.encode("utf-8").hex(),16)
    constant = 0x0123456789ABCDEFFEDCBA9876543210
    master_key_prime = master_key ^ constant

    #print("constant = ",constant)
    #print("master key prime = ",master_key_prime)


    """
    Séparation de K et K' en 8 blocs de 16 bits
    """
    for j in range(1,9):
        key[j] = (master_key >> (16 * (8 - j))) & 0xFFFF
        key_prime[j] = (master_key_prime >> (16 * (8 - j))) & 0xFFFF


    """
    Application de key scheduling présent page 15 et 16 du pdf de la norme Kasumi
    """
    for i in range(1, 9):

        """======================Sous clés Fonction L =======================
        On shift, mais on veut que ça boucle sur 16 bits, d'ou le OR"""
        keyL1[i] = ((key[i] << 1) & 0xFFFF) | (key[i] >> (16 - 1))
        """
        Condition ternaire pour le modulo : on veut que ça boucle de 1 à 8, donc si 8 mod 8, on change 0 par 8
        """
        keyL2[i] = key_prime[((i + 2) % 8) if ((i + 2) % 8) != 0 else 8]

        """======================Sous clés Fonction O =======================
        Ici, un savant mélange des deux :-))))"""
        keyO1[i] = ((key[((i+1) % 8) if ((i+1) % 8)!=0 else 8] << 5) & 0xFFFF) | (key[((i+1) % 8) if ((i+1) % 8)!=0 else 8] >> (16 - 5))
        keyO2[i] = ((key[((i+5) % 8) if ((i+5) % 8)!=0 else 8] << 8) & 0xFFFF) | (key[((i+5) % 8) if ((i+5) % 8)!=0 else 8] >> (16 - 8))
        keyO3[i] = ((key[((i+6) % 8) if ((i+6) % 8)!=0 else 8] << 13) & 0xFFFF) | (key[((i+6) % 8) if ((i+6) % 8)!=0 else 8] >> (16 - 13))

        """======================Sous clés Fonction I ======================="""
        keyI1[i] = key_prime[((i+4) % 8) if ((i+4) % 8)!=0 else 8]
        keyI2[i] = key_prime[((i+3) % 8) if ((i+3) % 8)!=0 else 8]
        keyI3[i] = key_prime[((i+7) % 8) if ((i+7) % 8)!=0 else 8]


    """print("=========================Génération des sous-clés====================")
    print(key)
    print("keyL1 ",keyL1)
    print("keyL2 ",keyL2)
    print("key01 ",keyO1)
    print("key02 ",keyO2)
    print("key03 ",keyO3)
    print("keyI1 ",keyI1)
    print("keyI2 ",keyI2)
    print("keyI3 ",keyI3)
    print("=========================Fin de génération des sous-clés====================")"""



def encryptKasumi(mode, pkey):
    """
    Encrypte le contenu du fichier input.txt en suivant l'algorithme KASUMI tel que décrit dans la norme fournie par le 3GPP
    et en ajoutant les modifications GS15
    :param mode: String. Mode de chiffrement (ECB,CBC,PCBC)
    :param pkey: String. Clé de chiffrement
    :return: Void. Écrit le message crypté dans crypted.txt
    """

    setkeys(pkey)

    if (mode == "ecb"):
        print("\n=========== Chiffrement en mode ECB ==============\n")
    elif (mode == "cbc"):
        print("\n=========== Chiffrement en mode CBC ==============\n")
    elif (mode == "pcbc"):
        print("\n=========== Déchiffrement en mode PCBC ==============\n")
    else:
        print(("\nMode de Chiffrement inconnu, autodestruction enclenchée").upper())
        print("Boum")
        sys.exit()

    try:
        input = open("./files/input.txt", "r").read()
    except FileNotFoundError:
        print("Le fichier est introuvable !")
        sys.exit()

    input = bytearray(input,'utf-8')
    print("Message initial : ", input)

    """
    Sépare le message en blocs de 64 bits
    """
    input = [input[i: i + 8] for i in range(0, len(input), 8)]

    """
    Création du bytearray permettant de stocker le message crypté et du bytearray permettant de stocker le bloc 
    précédent (pour les modes de chiffrement)
    """
    res = bytearray('' , 'utf-8')
    previousbyte = bytearray()

    """
    Boucle pour effectuer le chiffrement sur chaque bloc de texte
    """
    for index, bloc in enumerate(input):

        """
        Gestion des différents modes de chiffrement
        """
        blocInitial = bloc
        if (mode == "cbc") or (mode == "pcbc"):
            """
            Première itération : On utilise le vecteur d'initialisation : une partie de la clé (le milieu ici)
            Itération suivantes : On xor le bloc actuel avec le précédent bloc chiffré (pour le mode pcbc, le bloc
            actuel est xoré avec le précédent bloc chiffré, lui-même déjà xoré avec le bloc initial précédent)
            """
            if (index == 0):
                bloc = bytesxor(bloc, master_key.to_bytes(16,"big")[4:12])
            else:
                bloc = bytesxor(previousbyte ,bloc)


        #print("Bloc: ", bloc)
        L = [""] * (9)
        R = [""] * (9)

        """
        Sépare le bloc actuel en 2 parties de 32 bits
        """
        L[0] = bloc[0:4]
        R[0] = bloc[4:8]

        #print("L Initial: ", L[0])
        #print("R Initial: ", R[0])

        """
        8 itérations de Feistel
        """
        for i in range(1, 9):
            R[i] = L[i - 1]

            """
            Test pour voir si l'itérateur est pair ou impair
            """
            if (i%2 == 0):
                L[i] = bytesxor(Fpair(L[i - 1],i), R[i - 1])
            else:
                L[i] = bytesxor(Fimpair(L[i - 1],i), R[i - 1])

            #print("L", i, " = ", L[i])
            #print("R", i, " = ", R[i])

        """
        Mise en mémoire du bloc chiffré pour le xorer avec le bloc suivant
        """
        previousbyte = bytearray()
        previousbyte.extend(L[8])
        previousbyte.extend(R[8])

        if (mode=="pcbc"):
            """
            Xore le bloc inital avec le bloc chiffré actuel, stocké dans previousByte
            """
            previousbyte = bytesxor(previousbyte, blocInitial)

        res.extend(L[8])
        res.extend(R[8])
        """Fin de la boucle For à 8 itérations"""

    print("Message Crypté : ", res)
    f = open("./files/crypted.txt", "wb")
    f.write(res)
    f.close()
    return " "



def decryptKasumi(mode,pkey):
    """
    Decrypte le contenu du fichier crypted.txt en suivant l'algorithme KASUMI tel que décrit dans la norme fournie par le 3GPP
    et en ajoutant les modifications GS15
    :param mode: String. Mode de chiffrement (ECB,CBC,PCBC)
    :param pkey: String. Clé de chiffrement
    :return: Void. Écrit le message decrypté dans decrypted.txt
    """

    setkeys(pkey)

    if (mode == "ecb"):
        print("\n=========== Déchiffrement en mode ECB ==============\n")
    elif (mode == "cbc"):
        print("\n=========== Déchiffrement en mode CBC ==============\n")
    elif (mode == "pcbc"):
        print("\n=========== Déchiffrement en mode PCBC ==============\n")
    else:
        print(("\nMode de déchiffrement inconnu, autodestruction enclenchée").upper())
        print("Pouf")
        sys.exit()



    input = open("./files/crypted.txt", "rb").read()
    input = bytearray(input)
    print("Message à décrypter : ", input)

    """
    Sépare le message en blocs de 64 bits
    """
    input = [input[i: i + 8] for i in range(0, len(input), 8)]

    """
    Création du bytearray permettant de stocker le message crypté et du bytearray permettant de stocker le bloc 
    précédent (pour les modes de chiffrement)
    """
    res = bytearray('' , 'utf-8')
    previousbyte = bytearray()
    """
    Boucle pour effectuer le chiffrement sur chaque bloc de texte
    """
    for index, bloc in enumerate(input):

        #print("Bloc: ", bloc)
        L = [""] * (9)
        R = [""] * (9)

        """
        Sépare chaque bloc en 2 parties de 32 bits
        """
        L[8] = bloc[0:4]
        R[8] = bloc[4:8]

        #print("L Initial: ", L[8])
        #print("R Initial: ", R[8])

        """
        8 itérations de Feistel en sens inverse
        """
        for i in range(8, 0, -1):
            L[i-1] = R[i]

            """
            Test pour voir si l'itérateur est pair ou impair
            """
            if (i%2 == 0):
                R[i-1] = bytesxor(Fpair(R[i],i), L[i])
            else:
                R[i-1] = bytesxor(Fimpair(R[i],i), L[i])

            #print("R", i-1, " = ", R[i-1])
            #print("L", i-1, " = ", L[i-1])

        """
        Gestion du déchiffrement des différents modes de chiffrement
        """
        if (mode == "cbc") or (mode == "pcbc"):
            """
            Première itération : On utilise le vecteur d'initialisation : une partie de la clé (le milieu ici)
            Itération suivantes : on xor le bloc déchiffré actuel avec le précédent bloc chiffré (pour le mode pcbc, le 
            bloc déchiffré  actuel est xoré avec le précédent bloc chiffré, lui-même déjà xoré avec le bloc déchiffré précédent)
            """
            if (index == 0):
                L[0] = bytesxor(L[0], master_key.to_bytes(16,"big")[4:8])
                R[0] = bytesxor(R[0], master_key.to_bytes(16,"big")[8:12])
            else:
                L[0] = bytesxor(L[0], previousbyte[0:4])
                R[0] = bytesxor(R[0], previousbyte[4:8])

        if (mode=="pcbc"):
            """
            Xore le bloc inital avec le bloc chiffré actuel, stocké dans previousByte
            """
            blocChiffreActuel = bytearray(L[0])
            blocChiffreActuel.extend(R[0])
            bloc = bytesxor(blocChiffreActuel, bloc)


        """
        Mise en mémoire du bloc chiffré actuel pour le xorer avec le bloc déchiffré suivant
        """
        previousbyte = bytearray()
        previousbyte.extend(bloc)

        res.extend(L[0])
        res.extend(R[0])
        """Fin de la boucle For à 8 itérations"""

    print("Message Decrypté : ", res)
    f = open("./files/decrypted.txt", "wb")
    f.write(res)
    f.close()

    return " "


def Fimpair(LeftPrecedent,i):
    """
    Fonction F principale quand l'itération est 1,3,5 ou 7
    :param LeftPrecedent: Byte. Partie gauche de l'itération précédente du message (partie droite suivante pour le déchiffrement)
    :param i: int. Indice d'itération de Kasumi
    :return: Bytearray. Résultat de la fonction Fo en 32 bits
    """
    return Fo(Fl(LeftPrecedent,i),i)

def Fpair(LeftPrecedent,i):
    """
    Fonction F principale quand l'itération est 2,4,6 ou 8
    :param LeftPrecedent: Byte. Partie gauche de l'itération précédente du message (partie droite suivante pour le déchiffrement)
    :param i: int. Indice d'itération de Kasumi
    :return: Bytearray. Résultat de la fonction Fl en 32 bits
    """
    return Fl(Fo(LeftPrecedent, i),i)

def Fo(bloc32,i):
    """
    Fonction de chiffrement Fo, sur 3 itérations
    :param bloc32: Byte. Bloc de charactères de 32 bits
    :param i: int. Indice d'itération de Kasumi
    :return: Bytearray. Bloc de charactères de 32 bits
    """
    """
    Récupération des clés sous forme de tableaux d'integers de 16 bits
    """
    global keyO1, keyO2, keyO3, keyI1, keyI2, keyI3
    res = bytearray('' , 'utf-8')
    LFo = [""]*4
    RFo = [""]*4

    """
    Sépare le bloc de 32bits en 2 parties de 16 bits
    """
    LFo[0] = bloc32[0:2]
    RFo[0] = bloc32[2:4]

    """
    Donne la valeur des bytes en integer, pour pouvoir effectuer les opérations binaires avec les clés
    """
    LFo[0] = int.from_bytes(LFo[0],"big")
    RFo[0] = int.from_bytes(RFo[0],"big")

    """
    3 itérations de la fonction FO
    """
    LFo[1] = RFo[0]
    RFo[1] = RFo[0] ^ Fi(LFo[1] ^ keyO1[i], keyI1[i], i)

    LFo[2] = RFo[1]
    RFo[2] = RFo[1] ^ Fi(LFo[2] ^ keyO2[i], keyI2[i], i)

    LFo[3] = RFo[2]
    RFo[3] = RFo[2] ^ Fi(LFo[3] ^ keyO3[i], keyI3[i], i)


    """
    Donne la valeur des integers gauches et droites en bytes et les stocke dans le bytearray res
    """
    res.extend(LFo[3].to_bytes(2,"big"))
    res.extend(RFo[3].to_bytes(2,"big"))

    #print("Fo",i," = ", res)
    return res

def Fi(bloc16, keyI, i):
    """
    Fonction de chiffrement Fi utilisée dans la fonction FO
    :param bloc16: Byte. Bloc de charactères de 16 bits
    :param keyI: Array. Sous-clé I correspondant à l'itération de la fonction FO
    :param i: Int. Indice d'itération de FO
    :return: Int. Résultat de la fonction FI (avec Modification #2) sur 16 bits
    """

    """
    Génération des Sboxs
    """
    S7 = generator.generateSBox1(str(master_key))
    S9 = generator.generateSBox2(str(master_key))

    """
    Séparation de la clé I en 2 integers de 8 bits
    """
    z1 = keyI >> 8
    z2 = keyI & 0xff

    """
    Application de la formule de Fi
    """
    res = bloc16 >> 2 ^ (S7[z1]|S9[z2])

    #print("Fi",i," = ", res)

    return res


def Fl(x,i):
    """
    Fonction de chiffrement FL
    :param x: Byte. Bloc de charactères de 32 bits (noté x sur le poly)
    :param i: int. Indice d'itération de Kasumi
    :return: Bytearray. Bloc de charactères de 32 bits
    """

    """
    Récupération des clés sous forme de tableaux d'integers de 16 bits
    """
    global keyL1, keyL2
    """
    Sépare le bloc de 32bits en 2 parties de 16 bits
    """
    lx = x[0:2]
    rx = x[2:4]

    """
    Application de la formule de la fonction FI (Le OR sert à faire boucler les bits avec le shift)
    """
    rx = inversion(int.from_bytes(rx,"big") ^ (((int.from_bytes(lx,"big") & keyL1[i]) << 1  & 0xFFFF) | ((int.from_bytes(lx,"big") & keyL1[i]) >> (16 - 1))))
    lx = inversion(int.from_bytes(lx,"big") ^ (((rx | keyL2[i]) << 1  & 0xFFFF) | (rx >> (16 - 1))))

    res = bytearray("", 'utf-8')
    res.extend(lx.to_bytes(2,"big"))
    res.extend(rx.to_bytes(2,"big"))
    #print(" Fl",i," = ", res)
    return (res)


def inversion(bloc16):
    """
    Opération d'inversion dans un corps de Galois
    :param bloc16: int. Bloc de texte à inverser
    :return: int. Bloc de texte inversé
    """
    F = ffield.FField(16)  # create the field GF(2^16)
    return F.Inverse(bloc16)

def bytesxor(s1,s2) :
    """
    xore chaque bit de deux octets
    :param s1: Byte. premier octet
    :param s2: Byte. deuxième octet
    :return: Byte. Octet xoré
    """
    return bytes(a ^ b for (a, b) in zip(s1, s2))

#encryptKasumi("ecb")
#decryptKasumi("ecb")