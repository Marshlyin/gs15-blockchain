import sys
import os

if os.name == "posix":
    clear = lambda: os.system("clear")
else: clear = lambda: os.system("cls")


def menu(path):
    hash = ""
    toprint=""
    choix=""
    if path == "./files/verif.txt" :
        choix = input(
            "Comment a été hashé le hash permettant de vérifier \"verif.txt\" ? \n->1<- Avec une fonction éponge \n->2<- Avec SHA-256\nVotre choix : ")
    elif path == "./files/tohash.txt" :
        choix = input(
            "Comment voulez vous hasher le fichier \"tohash.txt\" ? \n->1<- Hasher avec une fonction éponge \n->2<- Hasher avec SHA-256\nVotre choix : ")
    if choix == "1":
        n = input("Combien de tours pour la phase d'essorage ?")
        if n.isdigit() and n!=0:
            M = open(path, "r")
            hash = sponge(int(n), M.read())
            toprint = "La phase de d'essorage à tourné " + n + " fois\nMessage hashé utilisant une fonction éponge, couplée avec un SHA-256"
            M.close()
        else:
            print("Merci d'entrer un chiffre supérieur à 0")
    elif choix == "2":
        M = open(path, "r")
        hash = sha256(M.read())
        toprint = "Message hashé utilisant une fonction éponge, couplée avec un SHA-256"
        M.close()

    return hash, toprint

"""
La fonction a été implémentée en utilisant la norme décrite dans le pdf suivant :
https://csrc.nist.gov/csrc/media/publications/fips/180/4/final/documents/fips180-4-draft-aug2014.pdf
"""

K = (0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
     0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5,
     0xd807aa98, 0x12835b01, 0x243185be, 0x550c7dc3,
     0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
     0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
     0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da,
     0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
     0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967,
     0x27b70a85, 0x2e1b2138, 0x4d2c6dfc, 0x53380d13,
     0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
     0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3,
     0xd192e819, 0xd6990624, 0xf40e3585, 0x106aa070,
     0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
     0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3,
     0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
     0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2)


def rightShift(x, n):
    """
    Décale les bits du mot x de n bit vers la droite
    :param x: Int. mot à décaler
    :param n: Int. Nombre de décalages à appliquer
    :return: Int. Le mot décalé
    """
    return x >> n & 0xFFFFFFFF

def rotateRight(x,n):
    """
    Permutation circulaire du mot x de n bits vers la droite
    :param x: Int. mot à décaler
    :param n: Int. Nombre de décalages à appliquer
    :return: Int. Le mot décalé
    """
    return ((x >> n ) | (x << (32 - n))) & 0xFFFFFFFF

"""
Définition des fonction utilisées dans SHA-256
"""
def ch(x,y,z):
    return (x & y) ^ (~x & z)

def maj(x,y,z):
    return (x & y) ^ (x & z) ^ (y & z)

def sigmaMin0(x):
    return rotateRight(x, 7) ^ rotateRight(x, 18) ^ rightShift(x,3)

def sigmaMin1(x):
    return rotateRight(x, 17) ^ rotateRight(x, 19) ^ rightShift(x,10)

def sigmaMaj0(x):
    return rotateRight(x, 2) ^ rotateRight(x, 13) ^ rotateRight(x,22)

def sigmaMaj1(x):
    return rotateRight(x, 6) ^ rotateRight(x, 11) ^ rotateRight(x,25)


"""
PREPROCESS
"""
"""
Initialisation des valeurs de H
"""
H = (0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
     0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19)

"""
SHA-256
"""
def pad(message):
    """
    Pad le message en multiples de 512 bits, voir page 13 du pdf, section 5.1.1)
    :param message: String. Le message à hasher
    :return: String. Le message paddé en bits
    """
    l = len(message)*8
    k = 448 - (l + 1) % 512
    bitstoAdd = bin(l)[2:].zfill(k+64)
    padded = ''.join('{0:08b}'.format(ord(x), 'b') for x in message) + "1" + bitstoAdd
    #print("Message padé : ", (hex(int(padded,2))))
    #print("Taille message padé (doit être un multiple de 512) : ", (len(hex(int(padded,2))))*4)
    return (padded)


def parse(paddedmessage):
    """
    Parse le message en blocs de 512 bits sous la forme de 16 blocs de 32 bits
    :param paddedmessage: String. Message paddé sous la forme d'un string de bits
    :return: Int Array. Tableau de N blocs de 512 bits, eux-même des tableaux de 16 entiers de 32 bits
    """

    """
    Séparation du message en blocs de 512 bits
    """
    paddedmessage = [paddedmessage[i: i + 512] for i in range(0, len(paddedmessage), 512)]
    M = [None] * (len(paddedmessage) + 1)

    """
    Séparation de chaque bloc en 16 blocs de 32 bits
    """
    for index,bloc in enumerate(paddedmessage):
        Mi = [None] * 16
        for i in range(0,16):
            Mi[i] = bloc[i*32:(i+1)*32]
            Mi[i] = int(Mi[i], 2) & 0xFFFFFFFF
            #print(Mi[i])
        M[index+1] = Mi

    #print("Message parsé (N bloc de 512 sous la forme de 16 blocs de 32 bits) : ", M)
    return (M)

"""
END PREPROCESS
"""

def sha256(M):
    """
    Fonction SHA-256 telle que décrite dans la norme https://csrc.nist.gov/csrc/media/publications/fips/180/4/final/documents/fips180-4-draft-aug2014.pdf
    :param M: String. Message à hasher
    :return: String. Hash du message en 256 bits
    """

    global K

    H = (0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
         0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19)

    M = parse(pad(M))

    """
    Application de la compression pour chaque bloc de 512 bits
    """
    for i in range(1,len(M)):
        Wt = [None] * 64
        """
        Mise en place de Wt
        """
        for t in range(0,64):
            if t <= 15:
                Wt[t] = M[i][t]
            else:
                Wt[t] = sigmaMin1(Wt[t-2]) + Wt[t-7] + sigmaMin0(Wt[t-15]) + Wt[t-16]  & 0xFFFFFFFF

        #print("Wt = ", Wt)
        """
        Initialisation des variables a,b,c,d,e,f, g et h
        """
        a,b,c,d,e,f,g,h = H[0], H[1], H[2], H[3], H[4], H[5], H[6], H[7]

        """
        Application de l'algorithme
        """
        for t in range(0,64):
            t1 = h + sigmaMaj1(e) + ch(e, f, g) + K[t] + Wt[t] & 0xFFFFFFFF
            t2 = sigmaMaj0(a) + maj(a, b, c) & 0xFFFFFFFF
            h = g
            g = f
            f = e
            e = d + t1 & 0xFFFFFFFF
            d = c
            c = b
            b = a
            a = t1 + t2 & 0xFFFFFFFF

        H = (a + H[0] & 0xFFFFFFFF, b + H[1] & 0xFFFFFFFF, c + H[2] & 0xFFFFFFFF, d + H[3] & 0xFFFFFFFF, e + H[4] & 0xFFFFFFFF, f + H[5] & 0xFFFFFFFF, g + H[6] & 0xFFFFFFFF, h + H[7] & 0xFFFFFFFF)

    result = ""
    for i in H:
        #print(i, " : " , hex(int(bin(i)[2:].zfill(32),2))[2:].zfill(8))
        result += str(hex(int(bin(i)[2:].zfill(32),2))[2:].zfill(8))

    #print(result)
    return result

"""
FIN SHA-256
"""

"""
SPONGE
"""
def sponge(N, message):
    """
    Fonction éponge utilisant une fonction de hashage SHA-256 modifiée (sha256Output1600())
    :param N: Int. Nombre de tours de la phase de récupération
    :param message: String. Le message à hasher
    :return: String. Le message hashé sur 256 bits
    """

    r = 1088
    c = 512
    etat = [0]*(r+c)

    """
    On pad le message pour avoir un multiple de 1088 bits puis on le parse en blocs de 1088 bits
    """
    padded = padSponge(message)
    parsed = [padded[i: i + 1088] for i in range(0, len(padded), 1088)]

    """ABSORPTION"""
    for block in parsed:
        """
        On XORe la partie [0:r] de l'état avec la partie du message actuelle
        """
        for i in range(0, r):
            etat[i] = etat[i] ^ int(block[i],2)
        """
        On applique la fonction à l'entiereté du tableau état (ici, un SHA-256 modifié pour retourner
        un hash de r+c=1600 bits) 
        """
        fEtat = sha256Output1600(decode_binary_string(''.join(str(e) for e in etat)))
        for index, j in enumerate(etat):
            etat[index] = int(fEtat[index], 2)
        #print(len(etat))

    """ESSORAGE"""
    """
    On applique la phase d'essorage autant de fois que l'utilisateur l'a indiqué
    """
    for i in range(0, N):
        fEtat = sha256Output1600(decode_binary_string(''.join(str(e) for e in etat)))
        for index, j in enumerate(etat):
            etat[index] = int(fEtat[index], 2)
        #print(len(etat))
    result = ''.join(str(e) for e in etat)
    """
    On tronque le résultat pour avoir un hash de 256 bits
    """
    result = result[0:256]
    #return str(format(int(result,2),'x'))
    return str(hex(int(result,2))[2:].zfill(64))


def padSponge(message):
    """
    Pad le message en multiples de 1088 bits, pour correspondre au r de l'état de la fonction éponge
    :param message: String. Le message à hasher
    :return: String. Le message paddé en 1088 bits
    """
    l = len(message)*8
    k = 1024 - (l + 2) % 1088
    bitstoAdd = bin(l)[2:].zfill(k+64)
    """
    Comme pour le SHA-256, on ajoute la longueur du message en bits à la fin du message paddé, ici, on ajoute également
    un bit 1 à la fin pour éviter d'avoir le dernier bit nul
    """
    padded = ''.join('{0:08b}'.format(ord(x), 'b') for x in message) + "1" + bitstoAdd + "1"
    return (padded)


def sha256Output1600(M):
    """
    Fonction SHA-256 telle que décrite dans la norme https://csrc.nist.gov/csrc/media/publications/fips/180/4/final/documents/fips180-4-draft-aug2014.pdf,
    modifiée pour sortir un hash sur 1792 bits qui sera ensuite tronqué sur 1600 bits
    :param M: String. Message à hasher
    :return: String. Hash du message en 1600 bits
    """
    global K

    H = (0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
         0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19)


    M = parse(pad(M))
    """
    Application de la compression pour chaque bloc de 512 bits
    """
    for i in range(1,len(M)):
        Wt = [None] * 64
        """
        Mise en place de Wt
        """
        for t in range(0,64):
            if t <= 15:
                Wt[t] = M[i][t]
            else:
                Wt[t] = sigmaMin1(Wt[t-2]) + Wt[t-7] + sigmaMin0(Wt[t-15]) + Wt[t-16]  & 0xFFFFFFFF

        #print("Wt = ", Wt)
        """
        Initialisation des variables a,b,c,d,e,f,g et h
        """
        a,b,c,d,e,f,g,h = H[0], H[1], H[2], H[3], H[4], H[5], H[6], H[7]

        """
        Application de l'algorithme
        """
        for t in range(0,64):
            t1 = h + sigmaMaj1(e) + ch(e, f, g) + K[t] + Wt[t] & 0xFFFFFFFF
            t2 = sigmaMaj0(a) + maj(a, b, c) & 0xFFFFFFFF
            h = g
            g = f
            f = e
            e = d + t1 & 0xFFFFFFFF
            d = c
            c = b
            b = a
            a = t1 + t2 & 0xFFFFFFFF


        """
        Pour sortir 1792 bits, on applique le calcul de H 7 fois (7*256 = 1792) et on ajoute les résultats de H pour avoir le hash
        """
        H = (a + H[0] & 0xFFFFFFFF, b + H[1] & 0xFFFFFFFF, c + H[2] & 0xFFFFFFFF, d + H[3] & 0xFFFFFFFF, e + H[4] & 0xFFFFFFFF, f + H[5] & 0xFFFFFFFF, g + H[6] & 0xFFFFFFFF, h + H[7] & 0xFFFFFFFF)
        H2 = (b + H[0] & 0xFFFFFFFF, c + H[1] & 0xFFFFFFFF, d + H[2] & 0xFFFFFFFF, e + H[3] & 0xFFFFFFFF, f + H[4] & 0xFFFFFFFF, g + H[5] & 0xFFFFFFFF, h + H[6] & 0xFFFFFFFF, a + H[7] & 0xFFFFFFFF)
        H3 = (c + H[0] & 0xFFFFFFFF, d + H[1] & 0xFFFFFFFF, e + H[2] & 0xFFFFFFFF, f + H[3] & 0xFFFFFFFF, g + H[4] & 0xFFFFFFFF, h + H[5] & 0xFFFFFFFF, a + H[6] & 0xFFFFFFFF, b + H[7] & 0xFFFFFFFF)
        H4 = (d + H[0] & 0xFFFFFFFF, e + H[1] & 0xFFFFFFFF, f + H[2] & 0xFFFFFFFF, g + H[3] & 0xFFFFFFFF, h + H[4] & 0xFFFFFFFF, a + H[5] & 0xFFFFFFFF, b + H[6] & 0xFFFFFFFF, c + H[7] & 0xFFFFFFFF)
        H5 = (e + H[0] & 0xFFFFFFFF, f + H[1] & 0xFFFFFFFF, g + H[2] & 0xFFFFFFFF, h + H[3] & 0xFFFFFFFF, a + H[4] & 0xFFFFFFFF, b + H[5] & 0xFFFFFFFF, c + H[6] & 0xFFFFFFFF, d + H[7] & 0xFFFFFFFF)
        H6 = (f + H[0] & 0xFFFFFFFF, g + H[1] & 0xFFFFFFFF, h + H[2] & 0xFFFFFFFF, a + H[3] & 0xFFFFFFFF, b + H[4] & 0xFFFFFFFF, c + H[5] & 0xFFFFFFFF, d + H[6] & 0xFFFFFFFF, e + H[7] & 0xFFFFFFFF)
        H7 = (g + H[0] & 0xFFFFFFFF, h + H[1] & 0xFFFFFFFF, a + H[2] & 0xFFFFFFFF, b + H[3] & 0xFFFFFFFF, c + H[4] & 0xFFFFFFFF, d + H[5] & 0xFFFFFFFF, e + H[6] & 0xFFFFFFFF, f + H[7] & 0xFFFFFFFF)

    result = ""
    for i in H:
        result += bin(i)[2:].zfill(32)
    for i in H2:
        result += bin(i)[2:].zfill(32)
    for i in H3:
        result += bin(i)[2:].zfill(32)
    for i in H4:
        result += bin(i)[2:].zfill(32)
    for i in H5:
        result += bin(i)[2:].zfill(32)
    for i in H6:
        result += bin(i)[2:].zfill(32)
    for i in H7:
        result += bin(i)[2:].zfill(32)

    #print("hash : ", result)
    return result

"""
FIN SPONGE
"""
def proofOfWork(message):
    """
    Fonction utilisée pour montrer le travail de l'ordinateur. Ici, on cherche un hash dont les 4 premiers caractères commencent par 0
    :param message: String. message à Hasher
    :return: Void. Print le hash voulu
    """
    choix = input(
        "Avec quel algorithme voulez vous effectuer une preuve de travail ? \n->1<- Avec la fonction éponge \n->2<- Avec SHA-256\nVotre choix : ")
    if choix == "1":
        n = input("Combien de tours pour la phase d'essorage ?")
        if n.isdigit() and n != 0:
            hash = sponge(int(n),message)
            i = 0
            while hash[0:4] != "0000" :
                hash = sponge(int(n),message+" "+str(i))
                print("On cherche un hash de la forme 0000xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
                print("Bonjour",i, ":",hash)
                clear()
                i+=1
            print("Bonjour", i - 1, ":", hash)
        else:
            print("Merci d'entrer un chiffre supérieur à 0")
    elif choix == "2":
        hash = sha256(message)
        i = 0
        while hash[0:4] != "0000" :
            hash = sha256(message+" "+str(i))
            print("On cherche un hash de la forme 0000xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
            print("Bonjour",i, ":",hash)
            clear()
            i+=1
        print("Bonjour", i-1, ":", hash)

def decode_binary_string(s):
    return ''.join(chr(int(s[i*8:i*8+8],2)) for i in range(len(s)//8))
