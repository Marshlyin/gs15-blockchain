import utils.hashage as hashage
from pathlib import Path
import os
import libnum
from math import gcd
import random
import re
import glob
import natsort

P =  12364280392709289256811428361725305690863898345873359319372625351473228431470523677095471486354544862414485369191001286838696326012684818673375499032858663
gen =  5


class User:
    def __init__(self, id, name, publicKey, privateKey):
        self.id = id
        self.name = name
        self.privateKey = privateKey
        self.publicKey = publicKey


    def sign(self, message, P, gen):
        """
        We use the Elgamal algorithm to sign the transaction in this blockchain
        :param message: the transaction
        :param P: the strong prime number, parameter of all the program
        :param gen: its generator
        :return: the signature which is a table of the two parts, R (0) and S (1)
        """
        k = random.randint(0, P - 2)
        hash = hashage.sha256(message)
        while gcd(k, P - 1) != 1:
            k = random.randint(0, P - 2)
        r = pow(gen, k, P)
        s = ((int(hash, 16) - self.privateKey * r) * libnum.invmod(k, P - 1)) % (P - 1)
        signature = r, s
        return signature

    def verifySign(self, signatureR, signatureS, message, P, gen):
        """
        We verify the signature using ElGamal algorithm
        :param signatureR: the R part of the signature
        :param signatureS: the S part of the signature
        :param message: the transaction
        :param P: the strong prime number, parameter of all the program
        :param gen: its generator
        :return: true if the transaction is signed by its rightfull owner, false otherwise
        """
        v_1 = (pow(self.publicKey, signatureR, P)*pow(signatureR,signatureS,P))%P
        hash = hashage.sha256(message)
        v_2 = pow(gen, int(hash, 16), P)
        if (v_1 == v_2):
            return True
        else:
            return False



class Transaction:

    def __init__(self, user1,user2, amount):
        self.user1ID = user1.id
        self.user2ID = user2.id
        self.amount = amount
        """
        La signature se génère elle-même lorsque les autres paramètres sont appliqués
        """
        self.signature = user1.sign(" { "+str(self.user1ID)+ " -> " +str(self.user2ID)+" :: "+str(self.amount) + " $$ } ", P, gen)

    def __str__(self):
        """
        Affichage d'une transaction lorsqu'on l'appelle avec un str()
        :return: String.
        """
        return "{ " + str(self.user1ID) + " -> " + str(self.user2ID) + " :: " + str(self.amount) + " $$ } -> *"+ str(self.signature[0])+ "/" + str(self.signature[1])+"#"


class Block:
    def __init__(self, id, transactions, previousHash, hash=None, salt=None):
        self.id = id
        self.transactions = transactions
        self.previousHash = previousHash
        """
        Si le hash et le sel sont renseignés, on les attribue à l'objet, sinon, on les calcule grâce à hashBlock() 
        """
        if (not hash and not salt):
            self.hash, self.salt = self.hashBlock()
        else:
            self.hash, self.salt = hash, salt

    def __str__(self):
        """
        Affichage d'un bloc lorsqu'on l'appelle avec un str()
        :return: String.
        """
        return 'Block : '+str(self.id)


    def hashBlock(self):
        """
        Refonte de la fonction hashage.proofOfWork() pour la blockchain. Génère un hash finissant par quatre 0 en incrémentant un sel
        :return: String, String: retourne le hash et le sel générés
        """
        message = ''.join(self.transactions) + self.previousHash
        hash = hashage.sha256(message)
        salt = 0
        print("Creating block")
        while hash[-4:] != "0000":
            hash = hashage.sha256(message + str(salt))
            print("\r",("."*(salt%4)), end ="")
            salt += 1
        print("\nMessage : ", message, "\nSalt : ",str(salt-1), "\nHash : ",hash)
        return hash, str(salt-1)



class Blockchain:
    """
    Création de la blockchain lors de l'instantiation de la classe
    """
    blocks = []

    def addBlock(self,id, transactions, previousHash):
        """
        Créé un bloc et l'ajoute à la blockchain. Le bloc va générer lui même son hash et son sel
        :param id: Int. id du bloc
        :param transactions: Liste. Liste de transactions sous forme de strings
        :param previousHash: String. Hash du bloc précédent
        :return: Void. Ajoute le bloc à la blockchain et créé le fichier correspondant
        """
        newBlock = Block(id, transactions, previousHash)
        self.saveBlocksToFile(newBlock)
        self.blocks.append(newBlock)

    def addBlockFromFile(self, path):
        """
        Créé les blocs existants dans les fichiers (blockchain/*.txt) et les ajoute à la blockchain
        :param path: String. Chemin vers les fichiers de blocs
        :return: Void.
        """
        f = open(path, "r")
        """
        Gestion des OS windows et Linux
        """
        if os.name == "posix":
            id = str(path).strip("./blockchain/")[:-4]
        elif os.name =="nt":
            id = str(path).strip("./blockchain\\")[:-4]
        """
        Sépare les lignes du fichier pour récupérer, recalculer, et insérer les informations de chaque bloc dans l'objet Bloc()
        """
        lines = f.read().splitlines()
        transactions = lines[0].split(",")
        previousHash = lines[1]
        salt = lines[2]
        hash = hashage.sha256(''.join(transactions) + previousHash + salt)
        self.blocks.append(Block(id, transactions, previousHash, hash, salt))
        f.close()

    def saveBlocksToFile(self, blocks):
        """
        Enregistre les blocs de la blockchain dans des fichiers texte
        :param blocks: Block Liste. Liste des blocks de la blockchain
        :return: Void.
        """
        f = open("./blockchain/"+str(blocks.id)+".txt","w")
        f.write(','.join(blocks.transactions)+"\n"+blocks.previousHash+"\n"+str(blocks.salt))
        f.close()


    def initBlockchain(self):
        """
        Récupère la liste des fichiers de blocks et lance l'ajout de chaque blocs à la blockchain (un bloc doit donc déjà être présent dans le dossier blockchain/)
        :return: Void.
        """
        blckchn = glob.glob("./blockchain/*" + ".txt")
        blckchn = natsort.natsorted(blckchn)
        for block in blckchn:
            self.addBlockFromFile(block)


    def createNewBlock(self, users):
        """
        Créé les paramètres d'un block en créeant des transactions. Chaque transaction est créée en choisiant 2 utilisateurs et le montant de la transaction.
        Lorsqu'une ou plusieurs transction sont créées, lance la fonction addBlock() qui va crééer le bloc et l'ajouter à la blockchain
        :param users: User Liste. Liste des utilisateurs de la blockchain
        :return: Void.
        """
        addtransaction = True
        addBlock = True
        transactions = []
        """
        Création de la liste de transactions
        """
        while addtransaction:
            """
            Sélection de l'emmeteur
            """
            print("Séléctionnez l'émetteur du virement : \n")
            i = 0
            emetteur = ""
            destinataire = ""
            for user in users:
                print(str(user.id)+". "+user.name)
                i+=1
            user1 = input(": ")
            if user1.isdigit() and user1>="0" and user1<=str(i) :
                emetteur = users[int(user1)]
                print(emetteur.name," est l'émetteur")
            else:
                print("L'indice renseigné pour l'emetteur n'est pas valide")
                addBlock = False
                break
            """
            Sélection du destinataire
            """
            print("Séléctionnez le destinataire du virement : \n")
            i = 0
            for user in users:
                if user != users[int(user1)]:
                    print(str(user.id)+". "+user.name)
                    i+=1
            user2 = input(": ")
            if user2.isdigit() and user2>="0" and user2<=str(i) and user2 != user1 :
                destinataire = users[int(user2)]
                print(destinataire.name," est le destinataire")
            else:
                print("L'indice renseigné pour le destinataire n'est pas valide")
                addBlock = False
                break
            """
            Séléction du montant
            """
            amount = input("Entrez le montant de la transaction : ")
            if not amount.isdigit():
                print("Merci d'entrer un nombre")
            """
            Création de la liste de transactions
            """
            transactions.append(str(Transaction(emetteur, destinataire, amount)))
            yesno = input("Voulez vous entrer une autre transaction ? (y/n)")
            if(yesno!="y" and yesno!="n" and yesno!="Y"and yesno!="N"):
                print("entrée invalide")
                addtransaction = False
            else:
                if(yesno == "n" or yesno=="N"):
                    addtransaction = False

        if (len(self.blocks) == 0):
            """
            Si la blockchain est vide, on donne l'id 0 au bloc et on lui donne un hash par défaut (SHA-256 de "")
            """
            id = 0
            previousHash = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        else:
            """
            Sinon, on séléctionne le bloc précédent dans la blockchain et on calcule son hash pour l'ajouter au bloc actuel en tant que Hash précédent
            """
            previousBlock = self.blocks[len(self.blocks)-1]
            id = (int(previousBlock.id)+1)
            previousHash = hashage.sha256(''.join(previousBlock.transactions) + previousBlock.previousHash + previousBlock.salt)
        """
        Création du bloc et ajout à la blockchain et aux dossiers si il n'y a pas eu d'erreurs
        """
        if addBlock: self.addBlock(id ,transactions, previousHash)
        else : print("Le bloc n'a pas été ajouté")


    def verifyBlockchain(self, users):
        """
        Vérifie l'intégrité de la blockchain en parcourant tous les blocs, recalculant leurs hashs et les signatures
        de leurs transactions et comparaison avec les blocs suivants
        :param users: User Liste. Liste des utilisateurs de la blockchain
        :return: Void.
        """
        for index, block in enumerate(self.blocks):
            if (index + 1 != len(self.blocks)):
                print("====================== Bloc", block.id, "======================")
                print("------- Vérification des signatures -------")
                for transaction in block.transactions:
                    """
                    Récupération des informations de chaque transaction dans les fichiers
                    """
                    idUser1 = int(transaction[2])
                    signataire = users[idUser1]
                    star, slash, diese = transaction.find("*"), transaction.find("/"), transaction.find("#")
                    signatureR = transaction[star + 1:slash]
                    signatureS = transaction[slash + 1:diese]
                    if not signataire.verifySign(int(signatureR), int(signatureS)," " + transaction[:(transaction.find("}") + 2)], P, gen):
                        """
                        Si la vérification des signatures (voir User.verifySign()) échoue, la blockchain est corrompue
                        """
                        print("Signature corrompue au bloc", block.id, "\n")
                        break
                    else:
                        print("OK pour les signatures")
                """
                Calcul du hash du bloc actuel pour le comparer avec le previousHash du bloc suivant.
                Si les deux hashs sont différents, la blockchain est corrompue
                """
                hash = hashage.sha256(''.join(block.transactions) + block.previousHash + block.salt)
                print("\n------- Vérification du Hash -------\n", hash)
                if (hash != self.blocks[index + 1].previousHash):
                    print("BlockChain corrompue au bloc", block.id,
                          "\nLe hash calculé est différent du hash stocké dans le bloc suivant (",self.blocks[index + 1].previousHash, ")")
                    break
                print("\n------- Sel :", block.salt, "-------\n")
            else:
                """
                On fait les opérations sur le dernier bloc ici pour éviter une erreur IndexOutOfRange avec le block suivant qui n'existe pas
                """
                print("====================== Dernier Bloc", index, "======================")
                print("------- Vérification des signatures -------")
                for transaction in block.transactions:
                    idUser1 = int(transaction[2])
                    signataire = users[idUser1]
                    star, slash, diese = transaction.find("*"), transaction.find("/"), transaction.find("#")
                    signatureR = transaction[star + 1:slash]
                    signatureS = transaction[slash + 1:diese]
                    if not signataire.verifySign(int(signatureR), int(signatureS),
                                                 " " + transaction[:(transaction.find("}") + 2)], P, gen):
                        print("Signature corrompue au bloc", block.id, "\n")
                        break
                    else:
                        print("OK pour les signatures")
                """
                Le hash n'est pas comparé avec le PreviousHash du bloc suivant puisqu'il n'y en a pas, mais on l'affiche 
                à titre informatif
                """
                hash = hashage.sha256(''.join(block.transactions) + block.previousHash + block.salt)
                print("\n------- Vérification du Hash -------\n", hash)
                print("\n------- Sel :", block.salt, "-------\n")
                print("LA BLOCKCHAIN EST VÉRIFIÉE")

