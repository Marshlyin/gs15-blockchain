import utils.generator as generator
import random

global P, gen

def menu(P, gen):
    """
    We use it to launch the function wanted
    :param P: the prime number
    :param gen: the generator
    :return:
    """
    choix = input("Que voulez vous faire ? \n->1<- Générer des nouveaux couples de clés privées / publiques \n->2<- Vérifier les clés déjà existantes\nVotre choix : ")
    if choix == "1":
        keysGeneration(P, gen)
    elif choix == "2":
        verifKeys()
    else :
        print("Entrée invalide; Réessayez")
        menu()

def generatePandGen():
    """
    We get a strong prime number of 512 bits from the file and we calculate one of its generator
    :return: the prime and one of its gen
    """
    P = generator.getPrimeFromFile()
    Gen = generator.findGeneratorStrong(P)
    return P, Gen

def keysGeneration(P, gen):
    """
    We create the private and public keys of alice and bob and store them in their file
    :param P: the strong prime number
    :param gen: its generator
    :return: /
    """
    print("P vaut : " + str(P))
    print("Le generateur vaut : "+ str(gen))
    print("Création des clés privées d'Alice et Bob : ")
    kprivAlice = random.randint(1, P-1)
    kprivBob = random.randint(1, P-1)
    print("La clé privée d'Alice est : "+str(kprivAlice))
    print("La clé privée de Bob est : "+str(kprivBob))
    print("Création des clés publiques d'Alice et Bob : ")
    kpubAlice = pow(gen, kprivAlice, P)
    kpubBob = pow(gen, kprivBob, P)
    print("La clé publique d'Alice est : "+str(kpubAlice))
    print("La clé publique de Bob est : "+ str(kpubBob))
    print("Enregistrement dans les fichiers correspondants...")

    with open("./files/Alicekey.txt", "w") as f:
        f.write("P : " + str(P) + "\nGen : "+ str(gen) + "\nPublic Key : " + str(kpubAlice) + "\nPrivate Key : " + str(kprivAlice))

    with open("./files/Bobkey.txt", "w") as f:
        f.write("P : " + str(P) + "\nGen : "+ str(gen) + "\nPublic Key : "+str(kpubBob)+"\nPrivate Key : " + str(kprivBob))

    return True

def regenPublicKey(name, P, gen, kpriv):
    """

    :param name: we regen the public key of the name called
    :param P: the strong prime number
    :param gen: its generator
    :param kpriv: the private key of the name
    :return: the file is saved with the new public key
    """
    kpub = pow(gen, kpriv, P)
    with open("./files/"+str(name)+"key.txt", "w") as f:
        f.write("P : " + str(P) + "\nGen : " + str(gen) + "\nPublic Key : " + str(kpub) + "\nPrivate Key : " + str(kpriv))


def verifKeys():
    """
    we verify the parameter P and Gen in the files stored, if they are not correct we generate them again
    we verify that the public keys are correct for each user or we generate again
    if the files doesnt exist we create them from scratch
    :return: the correct files
    """
    try:
        with open("./files/Alicekey.txt", "r") as fAlice:
            for ln in fAlice:
                if ln.startswith("P : "):
                    PAlice = int(ln[4:])
                elif ln.startswith("Gen : "):
                    GenAlice = int(ln[6:])
                elif ln.startswith("Public Key : "):
                    kpubAlice = int(ln[13:])
                elif ln.startswith("Private Key : "):
                    kprivAlice = int(ln[14:])

        with open("./files/Bobkey.txt", "r") as fBob:
            for ln in fBob:
                if ln.startswith("P : "):
                    PBob = int(ln[4:])
                elif ln.startswith("Gen : "):
                    GenBob = int(ln[6:])
                elif ln.startswith("Public Key : "):
                    kpubBob = int(ln[13:])
                elif ln.startswith("Private Key : "):
                    kprivBob = int(ln[14:])

        if ((PBob != PAlice) or (GenBob != GenAlice)) :
            print("Erreur dans le partage des clés, création d'une nouvelle base commune: ")
            keysGeneration()
            verifKeys()
        else :
            if kpubBob != pow(GenBob, kprivBob, PBob):
                print("Erreur dans la clé publique de Bob")
                regenPublicKey("Bob", PBob, GenBob, kprivBob)
                verifKeys()
            if kpubAlice != pow(GenAlice, kprivAlice, PAlice):
                print("Erreur dans la clé publique d'Alice")
                regenPublicKey("Alice", PAlice, GenAlice, kprivAlice)
                verifKeys()
            else :
                print("Les clés sont valides")
    except:
        print("Les fichiers n'existent pas. \n")
        keysGeneration()
        verifKeys()


def getPandGen():
    """
    We get P and gen from a trusted user, Alice
    If Alice doesnt know them (A.k.a the file doesnt exist), we create them from scratch
    :return: P and Gen
    """
    try:
        with open("./files/Alicekey.txt", "r") as fAlice:
            for ln in fAlice:
                if ln.startswith("P : "):
                    P = int(ln[4:])
                elif ln.startswith("Gen : "):
                    Gen= int(ln[6:])
        return P, Gen
    except FileNotFoundError:
        print("Les fichiers n'existent pas. \n")
        P, gen = generatePandGen()
        keysGeneration(P, gen)
        getPandGen()

def getAliceKeys():
    """
    We get the private key and the public key of alice
    :return: public key, private key
    """
    try:
        with open("./files/Alicekey.txt", "r") as fAlice:
            for ln in fAlice:
                if ln.startswith("Public Key : "):
                    kpubAlice = int(ln[13:])
                elif ln.startswith("Private Key : "):
                    kprivAlice = int(ln[14:])

        return kpubAlice, kprivAlice
    except:
        print("Alice a perdu ses clefs... \nCréations de nouvelles clefs pour alice et bob")
        keysGeneration()
        return getAliceKeys()

def getBobKeys():
    """
        We get the private key and the public key of Bob
        :return: public key, private key
    """
    try:
        with open("./files/Bobkey.txt", "r") as fBob:
            for ln in fBob:
                if ln.startswith("Public Key : "):
                    kpubBob = int(ln[13:])
                elif ln.startswith("Private Key : "):
                    kprivBob = int(ln[14:])

        return kpubBob, kprivBob
    except:
        print("Bob a perdu ses clefs... \nCréations de nouvelles clefs pour alice et bob")
        keysGeneration()
        getBobKeys()